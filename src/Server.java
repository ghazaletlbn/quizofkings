import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class Server {
    static String userPath = "Users" ;
    static int numberofusersfiles = 0;
    //static int numberOfActiveClients = 0;
    static ArrayList<User> users = new ArrayList<>();
    static HashMap<String,Integer> userposition = new HashMap<>(); //String for username , Integger for index
    static ArrayList<Subject> subjects = new ArrayList<>();
    static ArrayList<Game> games = new ArrayList<>();
    static ArrayList<ClientHandler> activeClients = new ArrayList<>();
    //loading users data and games data
    private  static void Loaddata() throws IOException, ClassNotFoundException {

     File userscountfile = new File(userPath+".txt");

        if(!userscountfile.exists()){
            System.out.println("is not exist");
            userscountfile.createNewFile();
        }
        else{
        FileInputStream fis = new FileInputStream(userscountfile);
        ObjectInputStream ois = new ObjectInputStream(fis);
        int userscount = (Integer) ois.readObject();
        for (int i = 0; i < userscount; i++) {
            File file = new File(userPath + i + ".txt");
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            User user = (User) objectInputStream.readObject();
            users.add(user);
            userposition.put(user.getUsername(), users.size() - 1);
        }

        }

    }
    //////
    public static void updateusercount() throws IOException {

        File file = new File(Server.userPath + ".txt") ;
        if(!file.exists()){
            System.out.println("is not exist");
            file.createNewFile();
        }
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        objectOutputStream.writeObject(Server.numberofusersfiles);
        objectOutputStream.flush();
    }

    //**********
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ServerSocket serverSocket = new ServerSocket(8850);
        Socket socket;
        Loaddata();
         while(true) {
             socket = serverSocket.accept();
             System.out.println("accept");
             ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
             ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

             Thread thread = new Thread(new ClientHandler(socket, ois, oos));
            // activeClients.add((ClientHandler) thread);
             thread.start();
         }


    }
}
