import java.io.*;
import java.net.Socket;

public class ClientHandler extends Thread {
    private static Socket socket;
    private static ObjectInputStream ois;
    private static ObjectOutputStream oos;

    static int numberOfActiveClients = 0;
    static String message;
    public ClientHandler(Socket socket, ObjectInputStream ois, ObjectOutputStream oos) {
       this.socket = socket;
       this.ois = ois;
       this.oos = oos;
    }
    @Override
    public void run(){
        try {
            System.out.println("client connected");
            message =ois.readUTF();
            String[] command = message.split(":");
            switch (command[0]){
                case "userChecker": {
                    userChecker(command);
                    System.out.println("checked");
                    break;
                }
                case "signUp": {
                    signUp(command);
                    break;
                }
                case "signIn": {
                    signIn(command);
                    break;
                }
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void userChecker(String[] parrams) throws IOException {
        String result = "userChecker:";
        if (!parrams[1].equals("")) {
            result = result.concat(parrams[1] + ":");
            if (Server.userposition.containsKey(parrams[1])) {
                result = result.concat("repeated");
            } else {
                result = result.concat("unique");
            }
            //System.out.println("In the user checker : " + result);
            oos.writeUTF(result);
            oos.flush();
            System.out.println("-- SERVER >>> " + result);
        } else {
            result = result.concat("empty:empty");
            oos.writeUTF(result);
            oos.flush();
            System.out.println(" -- SERVER >>> " + result);
        }
    }
    public void signUp(String[] parrams) throws IOException {
        if (Server.userposition.containsKey(parrams[1])) {
            oos.writeUTF("error:" + parrams[1] + ":repeatedUsername");
            oos.flush();
            System.out.println(" -- SERVER >>> " + "error:repeatedUsername");
        } else {
            User user = new User(parrams[1], parrams[2]);
            Server.users.add(user);
            Server.userposition.put(parrams[1], Server.users.size() - 1);
            userToFile(user);
            System.out.println("ClientHandler >>> Register success(signUp method)" + parrams[1] + "  " + parrams[2]);
            oos.writeUTF("signUp:" + parrams[1] + ":success");
            oos.flush();
            System.out.println(" ****result**** " + "signUp:success");
        }
    }
    public void signIn(String[] parrams) throws IOException {
        User user;
        if (Server.userposition.containsKey(parrams[1])) {
            user = Server.users.get(Server.userposition.get(parrams[1]));
            if (user.getPassword().equals(parrams[2])) {
                user.setlogin(true);
                oos.writeUTF("signIn:" + parrams[1] + ":success");
                oos.flush();
                oos.writeObject(user);
                oos.flush();
                System.out.println("*****TEST****** " + "signIn:" + parrams[1] + " success");
            } else {
                oos.writeUTF("signIn:" + parrams[1] + ":error");
                oos.flush();
                System.out.println(" -- SERVER >>> " + "signIn:" + parrams[1] + ":error");
            }
        } else {
            oos.writeUTF("signIn:" + parrams[1] + ":error");
            oos.flush();
            System.out.println(" -- SERVER >>> " + "signIn:" + parrams[1] + ":error");
        }


    }
    public void changeuser(String[] parrams) throws IOException {
        User user;
        if (Server.userposition.containsKey(parrams[1])) {
            user = Server.users.get(Server.userposition.get(parrams[1]));
            int position = Server.userposition.get(parrams[1]);
            if (Server.userposition.containsKey(parrams[2])) {
                oos.writeUTF("error:" + parrams[1] + ":repeatedUsername");
                oos.flush();
                System.out.println(" -- SERVER >>> " + "error:repeatedUsername");
            } else {
                user.setUsername(parrams[2]);
                Server.userposition.remove(parrams[1]);
                Server.userposition.put(parrams[2], position);
                userToFile(user);
                System.out.println("ClientHandler >>> UserChange success(signUp method)" + parrams[1] + "  " + parrams[2]);
                oos.writeUTF("userchange:" + parrams[2] + ":success");
                oos.flush();
                System.out.println(" ****result**** " + "signUp:success");
            }
        }
    }
    public static void userToFile(User object) {
        try {
            File file = new File(Server.userPath + Server.userposition.get(object.getUsername()) + ".txt") ;
            if(!file.exists()){
                System.out.println("is not exist");
                file.createNewFile();
                Server.numberofusersfiles++;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(object);
            objectOutputStream.flush();
            Server.updateusercount();
            System.out.println("USER SUCCESS !!!!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
